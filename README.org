#+TITLE: GCL -- Git Clone List

Inspired from [[https://gitlab.com/MindTooth/go-gcl][MindTooth/go-gcl]]. It will give you a list of owned and starred
projects from whichever GitLab provider you add, and from GitHub.


Example config for regular gitlab.com and github.
#+begin_src elisp
(setq gcl-gitlab-personal-token (+pass-get-secret "api/gitlab")
      gcl-github-personal-token (+pass-get-secret "api/github"))
#+end_src


To add more GitLab hosts, do the following:
#+begin_src elisp
(setq gcl-gitlab-domains `(("https://gitlab.com" . ,gcl-gitlab-personal-token)
                           ("https://gitlab.example.com" . "token"))
#+end_src

To call, simply run ~M-x gcl-clone~, and select the repository you want to
clone. You will also be prompted with a destination.
