;;; gcl.el --- Git Clone List -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2021 R0flcopt3r
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3.
;;
;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see <http://www.gnu.org/licenses/>.
;;
;; Author: R0flcopt3r <https://gitlab.com/R0flcopt3r>
;; Maintainer: R0flcopt3r
;; Created: February 27, 2021
;; Modified: February 27, 2021
;; Version: 0.0.1
;; Keywords:
;; Homepage: https://gitlab.com/R0flcopt3r/gcl.el
;; Package-Requires: ((emacs 27.1) (cl-lib "0.5") (request "20210214.37"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Get a searchable list of repositories on Gitlab and Github.
;;
;;; Code:
(require 'request)
(require 'magit)

(defvar gcl-gitlab-personal-token nil
  "Gitlab.com private token. For other domains see `gcl-gitlab-domains'.")
(defvar gcl-github-personal-token nil
  "Personal access token for GitHub.")

(defvar gcl-github-domain "https://api.github.com")
(defvar gcl-gitlab-domains `(("https://gitlab.com" . ,gcl-gitlab-personal-token))
  "Alist with gitlab host and gitlab private token.")

(defvar gcl--gitlab-base-endpoint "/api/v4")


(defun gcl--get-gitlab-projects (filter)
  "Return an alist of GitLab repositories based on the given FILTER. If FILTER
is starred, it will pull down all starred projects that the authenticated user
has."
  (let (result)
    (dolist (domain gcl-gitlab-domains result)
      (request (concat (car domain) gcl--gitlab-base-endpoint "/projects")
        :type "GET"
        :sync t
        :params `(("private_token" . ,(cdr domain))
                  ("order_by" . "updated_at")
                  (,filter . t))
        :success (cl-function (lambda (&key data &allow-other-keys) (setq result (vconcat data result))))
        :error (cl-function
                (lambda (&rest args &key error-thrown &allow-other-keys)
                  (message "Unexpected error: %S" error-thrown)))
        :parser #'json-read))
    (loop for repo across result
          collect `(,(concat "GitLab -- " (cdr (assoc 'name_with_namespace repo)))
                    . ,(cdr (assoc 'ssh_url_to_repo repo))))))


(defun gcl--get-github-projects (path)
  "Return alist of projects based on PATH. If PATH is `/user/repos' it will
return the repos owned by the authenitcated user"
  (let (result)
    (request (concat gcl-github-domain path)
      :type "GET"
      :sync t
      :headers `(("Content-Type" . "application/vnd.github.v3+json")
                 ("Authorization" . ,(concat "Bearer " gcl-github-personal-token)))
      :success (cl-function (lambda (&key data &allow-other-keys) (setq result data )))
      :error (cl-function
              (lambda (&rest args &key error-thrown &allow-other-keys)
                (message "Unexpected error: %S" error-thrown)))
      :parser #'json-read)
    (loop for repo across result
          collect `(,(concat "GitHub -- " (cdr (assoc 'full_name repo)))
                    . ,(cdr (assoc 'ssh_url repo))))))

(defun gcl-clone ()
  "Clone a repository selected from a list."
  (interactive)
  (let ((repos (append (gcl--get-gitlab-projects "owned")
                       (gcl--get-gitlab-projects "starred")
                       (gcl--get-github-projects "/user/repos")
                       (gcl--get-github-projects "/user/starred"))))
    (let ((repo (completing-read "repos: " repos)))
      (magit-clone-regular
       (cdr (assoc repo repos))
       (read-directory-name "Clone to: ")
       nil))))

(provide 'gcl)
;;; gcl.el ends here
